## Pipeline de dados do Aviation-Safety com delta-lake

Esse pipeline de dados foi desenvolvido com os dados do site: https://aviation-safety.net/  
tecnologia delta-lake ,pyspark 3.0.1 e jupyter-notebook. data-lake local com as seguintes
zonas:**`landing, bronze(raw), silver(trusted) e gold(refined)`**  
desenvolvedor: https://www.linkedin.com/in/romeritomorais/

**movendo os dados da zona landing para a bronze(raw)**

```python
from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.window import *
from pyspark.sql.functions import *
from delta import *
import databricks.koalas as ks

def display(x):
    df = ks.DataFrame(x)
    return df

core = 4
aplication = 'delta.application.aviation'

deltaSession = (
    SparkSession
    .builder
    .appName(f'{aplication}')
    .master(f'local[{core}]')
    .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
    .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
)

spark = configure_spark_with_delta_pip(deltaSession).getOrCreate()

landing = '/home/romerito/Dropbox/tecnology/develop/python/spark-deltalake/datalake/000000-landing-zone'
bronze  = '/home/romerito/Dropbox/tecnology/develop/python/spark-deltalake/datalake/000001-bronze-zone'
silver  = '/home/romerito/Dropbox/tecnology/develop/python/spark-deltalake/datalake/000002-silver-zone'
gold    = '/home/romerito/Dropbox/tecnology/develop/python/spark-deltalake/datalake/000003-gold-zone'

```  

carregando os dados do arquivo ***.json** em dataframe

```python
deltaAccidentsAviation = (
    spark
    .read
    .format('json')
    .option("path", f'{landing}/json/blob.json')
    .load()
)

# colunas do dataframe
deltaAccidentsAviation.columns
```  

adiciona coluna de ****`loadingdate`****

```python
deltaJsonTable = (
    deltaAccidentsAviation
    .withColumn("loadingdate", current_timestamp())
    .select(
        'json_table.*',
        'loadingdate'
    )
)
```

substituindo espaços em colunas por caracter `_`

```python
deltaReplaceTable = deltaJsonTable.toDF(*list(map(lambda x: x.replace(" ", "_"), deltaJsonTable.columns)))
```

escreve na zona bronze(raw) os dados brutos no formato delta

```python
(
    deltaReplaceTable
    .write
    .format('delta')
    .mode('append')
    .save(f'{bronze}/accidents')
)
```

## transformação da bronze-zone(raw) para silver-zone(trusted)

essa etapa le os dados da bronze-zone faz as transformações e salva os dados na silver-zone(trusted)

- essa é a nossa única fonte de verdade

criação de UDF(User Defined Function) para transformação de dados via sparkSQL e registrando em sessão  
as funções extraem do campo de date: dia da semana, mes, dia e ano.

```python
def day_of_the_week(string):
    if str is None:
        return None
    else:
        list = string.split()
        week_name = list[0]
        if week_name == 'sunday':
            week = 'domingo'
        elif week_name == 'monday':
            week = 'segunda-feira'
        elif week_name == 'tuesday':
            week = 'terca-feira'
        elif week_name == 'wednesday':
            week = 'quarta-feira'
        elif week_name == 'thursday':
            week = 'quinta-feira'
        elif week_name == 'friday':
            week = 'sexta-feira'
        elif week_name == 'saturday':
            week = 'sabado'
        else:
            week = 'null'
    return week


def split_date_month(string):
    if str is None:
        return None
    else:
        list = string.split()
        month_name = list[2]
        if month_name == 'january':
            month = '01'
        elif month_name == 'february':
            month = '02'
        elif month_name == 'march':
            month = '03'
        elif month_name == 'april':
            month = '04'
        elif month_name == 'may':
            month = '05'
        elif month_name == 'june':
            month = '06'
        elif month_name == 'july':
            month = '07'
        elif month_name == 'august':
            month = '08'
        elif month_name == 'september':
            month = '09'
        elif month_name == 'october':
            month = '10'
        elif month_name == 'november':
            month = '11'
        elif month_name == 'december':
            month = '12'
        else:
            month = 'null'
    return month


def split_date_day(string):
    if str is None:
        return None
    else:
        list = string.split()
        day = list[1]
        if len(day) == 2:
            concatenate = day
        else:
            concatenate = '0'+day
        return concatenate

    
def country_split(string):
    if str is None:
        return None
    else:
        lista = string.split(',')
        country = lista[2]
        remove_espacos = country.lstrip()
        return remove_espacos
    
    
spark.udf.register("split_date_month", split_date_month,StringType())
spark.udf.register("split_date_day", split_date_day,StringType())
spark.udf.register("day_of_the_week", day_of_the_week,StringType())
spark.udf.register("country_split", country_split,StringType())
```

carregando dados da camada bronze(raw)

```python
deltaBronzeTable = (
    spark
    .read
    .format("delta")
    .option('path', f'{bronze}/accidents')
    .load()
)
```

renomeando colunas do dataframe

```python
deltaColumnRenamed = (
    deltaBronzeTable
    .withColumnRenamed('Aircraft_damage:', 'aircraft_damage')
    .withColumnRenamed('Aircraft_fate:', 'aircraft_fate')
    .withColumnRenamed('C/n_/_msn:', 'msn')
    .withColumnRenamed('Collision_casualties:', 'collision_casualties')
    .withColumnRenamed('Crash_site_elevation:', 'crash_site_elevation')
    .withColumnRenamed('Crew:', 'crew')
    .withColumnRenamed('Cycles:', 'cycles')
    .withColumnRenamed('Date:', 'date')
    .withColumnRenamed('Departure_airport:', 'destination_airport')
    .withColumnRenamed('Destination_airport:', 'departure_airport')
    .withColumnRenamed('Engines:', 'engines')
    .withColumnRenamed('First_flight:', 'first_flight')
    .withColumnRenamed('Flightnumber:', 'flightnumber')
    .withColumnRenamed('Ground_casualties:', 'ground_casualties')
    .withColumnRenamed('Leased_from:', 'leased_from')
    .withColumnRenamed('Location:', 'location')
    .withColumnRenamed('Nature:', 'nature')
    .withColumnRenamed('On_behalf_of:', 'on_behalf_of')
    .withColumnRenamed('Operated_by:', 'operated_by')
    .withColumnRenamed('Operating_for:', 'operating_for')
    .withColumnRenamed('Operator:', 'operator')
    .withColumnRenamed('Passengers:', 'passengers')
    .withColumnRenamed('Phase:', 'phase')
    .withColumnRenamed('Registration:', 'registration')
    .withColumnRenamed('Status:', 'status')
    .withColumnRenamed('Time:', 'time')
    .withColumnRenamed('Total_airframe_hrs:', 'total_airframe_hrs')
    .withColumnRenamed('Total:', 'total')
    .withColumnRenamed('Type:', 'type')
)
```

criando dataframe a partir de outro filtrado

```python
deltaFilterTable = (
    deltaColumnRenamed
    .where(
        "date not like '%xxx%' and departure_airport not like '%?%' and departure_airport <> ''"
    )
)
```

pegando posição dentro de coluna para extrair o ano do primeiro voo

```python
deltaTransformColumnFlight =  (
    deltaFilterTable
    .withColumn('first_flight', substring('first_flight', 1,5))
)
```

criando novo dataframe com colunas selecionadas

```python
deltaSelectionColumns = (
    deltaTransformColumnFlight
    .select(
        "loadingdate",
        "aircraft_damage",
        "aircraft_fate",
        "msn",
        "crew",
        "collision_casualties",
        "crash_site_elevation",
        "cycles",
        "date",
        "departure_airport",
        "destination_airport",
        "engines",
        "first_flight",
        "flightnumber",
        "ground_casualties",
        "leased_from",
        "location",
        "nature",
        "on_behalf_of",
        "operated_by",
        "operating_for",
        "operator",
        "passengers",
        "phase",
        "registration",
        "status",
        "time",
        "total_airframe_hrs",
        "total",
        "type",
    )
)
```

convertendo colunas com lower()

```python
deltaColumnLowed = (
    deltaSelectionColumns
    .withColumn('crew', lower('crew'))
    .withColumn('aircraft_damage', lower('aircraft_damage'))
    .withColumn('aircraft_fate', lower('aircraft_fate'))
    .withColumn('date', lower('date'))
    .withColumn('departure_airport', lower('departure_airport'))
    .withColumn('destination_airport', lower('destination_airport'))
    .withColumn('engines', lower('engines'))
    .withColumn('location', lower('location'))
    .withColumn('nature', lower('nature'))
    .withColumn('operator', lower('operator'))
    .withColumn('passengers', lower('passengers'))
    .withColumn('phase', lower('phase'))
    .withColumn('registration', lower('registration'))
    .withColumn('status', lower('status'))
    .withColumn('type', lower('type'))
    .withColumn('total', lower('total'))
)
```

extraindo número de pessoas que foram a óbito de colunas do dataframe

```python
deltaObitPeople = (
    deltaColumnLowed
    .withColumn(
        "crew_fatalities", 
        when(substring(col("crew"),13,2).like("%/%") | 
        substring(col("crew"),13,2).like(""), "0")
        .otherwise(substring(col("crew"),13,2))
    )
    .withColumn(
        "crew_occupants", 
        when(substring(col("crew"),28,3).like("%/%") | 
        substring(col("crew"),28,3).like(""), "0")
        .otherwise(substring(col("crew"),28,3))
    )
    .withColumn(
        "passengers_fatalities", 
        when(substring(col("passengers"),13,3).like("%/%") | 
        substring(col("passengers"),13,3).like(""), "0")
        .otherwise(substring(col("passengers"),13,3))
    )
    .withColumn(
        "passengers_occupants", 
        when(substring(col("passengers"),-3,3).like("%/%") | 
        substring(col("passengers"),-3,3).like(""), "0")
        .otherwise(substring(col("passengers"),-3,3))
    )
)
```

substrindo caracteres especiais de colunas

```python
deltaReplaceCharacterPassengers = (
   deltaObitPeople
   .withColumn("passengers_fatalities", regexp_replace(regexp_replace(col("passengers_fatalities"),":",""),"s",""))
   .withColumn("passengers_occupants", regexp_replace(regexp_replace(col("passengers_occupants"),":",""),"s",""))
)
```

criando novas colunas com UDFs

```python
deltaColumnDate = (
    deltaReplaceCharacterPassengers
    .selectExpr("*", 
        "split_date_day(date) AS day_acident",
        "day_of_the_week(date) AS day_of_the_week",
        "split_date_month(date) AS month_acident",
        "substring(date,length(date)-4,5) AS year_acident"
    )

)
```

criação/concatenação de colunas

```python
deltaColumnTransform =  (
    deltaColumnDate
    .withColumn("flying_years", abs(col("first_flight") - col("year_acident")).cast(IntegerType()))
    .withColumn("year_of_accident", concat_ws('-',col("year_acident"),col("month_acident"),col("day_acident")))
)
```

passando valor por default caso o valor das colunas sejem vazias

```python
deltaCoalesceColumns = (
    deltaColumnTransform
    .withColumn("flying_years", coalesce(col("flying_years"), lit(0)))
    .withColumn("aircraft_damage", coalesce(col("aircraft_damage"), lit(0)))
    .withColumn("aircraft_fate", coalesce(col("aircraft_fate"), lit(0)))
    .withColumn("crew_fatalities", coalesce(col("crew_fatalities"), lit(0)))
    .withColumn("crew_occupants", coalesce(col("crew_occupants"), lit(0)))
    .withColumn("cycles", coalesce(col("cycles"), lit(0)))
    .withColumn("date", coalesce(col("date"), lit(0)))
    .withColumn("day_of_the_week", coalesce(col("day_of_the_week"), lit(0)))
    .withColumn("day_acident", coalesce(col("day_acident"), lit(0)))
    .withColumn("year_acident", coalesce(col("year_acident"), lit(0)))
    .withColumn("month_acident", coalesce(col("month_acident"), lit(0)))
    .withColumn("year_of_accident", coalesce(col("year_of_accident"), lit(0)))
    .withColumn("departure_airport", coalesce(col("departure_airport"), lit(0)))
    .withColumn("destination_airport", coalesce(col("destination_airport"), lit(0)))
    .withColumn("engines", coalesce(col("engines"), lit(0)))
    .withColumn("first_flight", coalesce(col("first_flight"), lit(0)))
    .withColumn("flightnumber", coalesce(col("flightnumber"), lit(0)))
    .withColumn("location", coalesce(col("location"), lit(0)))
    .withColumn("nature", coalesce(col("nature"), lit(0)))
    .withColumn("on_behalf_of", coalesce(col("on_behalf_of"), lit(0)))
    .withColumn("operated_by", coalesce(col("operated_by"), lit(0)))
    .withColumn("operating_for", coalesce(col("operating_for"), lit(0)))
    .withColumn("operator", coalesce(col("operator"), lit(0)))
    .withColumn("passengers_fatalities", coalesce(col("passengers_fatalities"), lit(0)))
    .withColumn("passengers_occupants", coalesce(col("passengers_occupants"), lit(0)))
    .withColumn("phase", coalesce(col("phase"), lit(0)))
    .withColumn("registration", coalesce(col("registration"), lit(0)))
    .withColumn("status", coalesce(col("status"), lit(0)))
    .withColumn("time", coalesce(col("time"), lit(0)))
    .withColumn("type", coalesce(col("type"), lit(0)))
)
```

cast/rename em colunas do dataframe

```python
deltaCastingColumns = (
    deltaCoalesceColumns
    .withColumn("data_ocorrencia_acidente", to_date(col("year_of_accident")))
    .withColumn("anos_de_voo", col("flying_years").cast(IntegerType()))
    .withColumn("numero_de_obitos_tripulantes", col("crew_fatalities").cast(IntegerType()))
    .withColumn("numero_de_tripulantes", col("crew_occupants").cast(IntegerType()))
    .withColumn("numero_obitos_passageiros", col("passengers_fatalities").cast(IntegerType()))
    .withColumn("passageiros", col("passengers_occupants").cast(IntegerType()))
    .withColumnRenamed("type", "tipo")
    .withColumnRenamed("first_flight", "ano_primeiro_voo")
    .withColumnRenamed("departure_airport", "aeroporto_de_partida")
    .withColumnRenamed("destination_airport", "aeroporto_de_destino")
    .withColumnRenamed("flightnumber", "numero_do_voo")
    .withColumnRenamed("phase", "estagio")
    .withColumnRenamed("registration", "cadastro")
    .withColumnRenamed("aircraft_damage", "danos_da_aeronave")
    .withColumnRenamed("aircraft_fate", "destino_da_aeronave")
    .withColumnRenamed("cycles", "ciclos")
    .withColumnRenamed("day_of_the_week", "dia_da_semana")
    .withColumnRenamed("day_acident", "dia_do_acidente")
    .withColumnRenamed("year_acident", "ano_do_acidente")
    .withColumnRenamed("month_acident", "mes_do_acidente")
    .withColumnRenamed("engines", "motores")
    .withColumnRenamed("location", "localizacao")
    .withColumnRenamed("nature", "natureza")
    .withColumnRenamed("on_behalf_of", "em_nome_de")
    .withColumnRenamed("operated_by", "operado_por")
    .withColumnRenamed("operating_for", "operando_para")
    .withColumnRenamed("operator", "operador")

)
```

selecionando colunas

```python
deltaColumnsTable = (
    deltaCastingColumns
    .select(
        "loadingdate",
        "data_ocorrencia_acidente",
        "tipo",
        "status",
        "ano_primeiro_voo",
        "anos_de_voo",
        "aeroporto_de_partida",
        "aeroporto_de_destino",
        "numero_do_voo",
        "estagio",
        "cadastro",
        "danos_da_aeronave",
        "destino_da_aeronave",
        "numero_de_obitos_tripulantes",
        "numero_de_tripulantes",
        "numero_obitos_passageiros",
        "passageiros",
        "ciclos",
        "dia_da_semana",
        "dia_do_acidente",
        "ano_do_acidente",
        "mes_do_acidente",
        "motores",
        "localizacao",
        "natureza",
        "em_nome_de",
        "operado_por",
        "operando_para",
        "operador"
    )
)
```

deduplicando dados pelo campo de data se inserção dos registros

```python
deltaDeduplicationTable = (
    deltaColumnsTable
    .withColumn("dense_rank", dense_rank().over(Window.partitionBy().orderBy(desc("loadingdate")))).where("ano_primeiro_voo > 0")
    .where("dense_rank=1")
    .select(
        "data_ocorrencia_acidente",
        "tipo",
        "status",
        "ano_primeiro_voo",
        "anos_de_voo",
        "aeroporto_de_partida",
        "aeroporto_de_destino",
        "numero_do_voo",
        "estagio",
        "cadastro",
        "danos_da_aeronave",
        "destino_da_aeronave",
        "numero_de_obitos_tripulantes",
        "numero_de_tripulantes",
        "numero_obitos_passageiros",
        "passageiros",
        "ciclos",
        "dia_da_semana",
        "dia_do_acidente",
        "ano_do_acidente",
        "mes_do_acidente",
        "motores",
        "localizacao",
        "natureza",
        "em_nome_de",
        "operado_por",
        "operando_para",
        "operador"
    )
)
```

escrendo na zona silver no formato delta

```python
(
    deltaDeduplicationTable
    .write
    .format('delta')
    .mode('overwrite')
    .partitionBy('ano_do_acidente')
    .save(f'{silver}/accidents')
)
```

## transformação da silver-zone(trusted)  para gold-zone(refined)

essa etapa pega os dados curados da silver-zone e cria data marths refinados prontas para a análise

```python
deltaSilverTable = (
    spark
    .read
    .format("delta")
    .option("path", f"{silver}/accidents")
    .load()
)
```

sumarindo dados

```python
deltaColumns = (
    deltaSilverTable
    .withColumn("total_de_obitos", col("numero_de_obitos_tripulantes") + col("numero_obitos_passageiros"))
    .select(
        "tipo",
        "natureza",
        "data_ocorrencia_acidente",
        "ano_primeiro_voo",
        "anos_de_voo",
        "total_de_obitos",
        "dia_da_semana",
        "dia_do_acidente",
        "mes_do_acidente",
        "ano_do_acidente"
    )
)
```

agregando dados selecionados

```python
deltaSummaryTable = (
    deltaColumns
    .groupBy(
        "ano_do_acidente", 
        "mes_do_acidente", 
        "dia_do_acidente", 
        "dia_da_semana")
    .agg(
        sum("total_de_obitos")
        .alias("total_de_obitos")
    )

)
```

escrevendo na zona gold o data marth de acidentes por periodo

```python
(
    deltaSummaryTable
    .write
    .format("delta")
    .mode("overwrite")
    .save(f"{gold}/datamarth_obitos_por_periodo")
)
```