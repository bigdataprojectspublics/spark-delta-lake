#!/usr/bin/env python
# coding: utf-8

# In[3]:


from pyspark.sql import SparkSession
from pyspark.sql.functions import lit, current_timestamp
from delta import *
from datetime import datetime
import os
import databricks.koalas as ks


def display(x):
    df = ks.DataFrame(x)
    return df


try:
    core = 2
    aplication = 'sparks.delta.application.landing'
    limit = 10
    deltaSession = (
        SparkSession
            .builder
            .appName(f'{aplication}')
            .master(f'local[{core}]')
            .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
            .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
    )
    spark = configure_spark_with_delta_pip(deltaSession).getOrCreate()
    landing_development = '/home/romerito/Documents/bigdata/spark-delta-lake/datalake/000000-landing-zone'
    bronze_development = '/home/romerito/Documents/bigdata/spark-delta-lake/datalake/000001-bronze-zone'
    landing_production = '/home/rundeck/spark-delta-lake/datalake/000000-landing-zone'
    bronze_production = '/home/rundeck/datalake/000001-bronze-zone'
    if os.path.isdir(f"{landing_production}"):
        # print("O diretório existe!")
        landing = landing_production
        bronze = bronze_production
    else:
        # print("O diretório não existe!")
        landing = landing_development
        bronze = bronze_development
except:
    print("erro ao carregar sessão do spark")

# In[2]:


# spark
# JetBrains


# carregando dados de .json da landing-zone  

# In[4]:


try:
    deltaAccidentsAviation = (
        spark
            .read
            .format('json')
            .option("path", f'{landing}/blob.json')
            .load()
    )
except:
    print(f"erro ao ingerir os dados")

# deltaAccidentsAviation.columns


# adiciona coluna de ****`data da carga`****  
# substituindo espaços em colunas por caracter `_`  
# imprimir schema do dataframe

# In[5]:


try:
    deltaJsonTable = (
        deltaAccidentsAviation
            .withColumn("loadingdate", current_timestamp())
            .select(
            'json_table.*',
            'loadingdate'
        )
    )
except:
    print(f"erro ao adicionar nova coluna")

# display(deltaJsonTable).head(limit)


# escreve dataframe da bronze-zone usando o formato ****`delta`**** usando o modo de inserção  

# In[6]:


try:
    deltaReplaceTable = deltaJsonTable.toDF(*list(map(lambda x: x.replace(" ", "_"), deltaJsonTable.columns)))
except:
    print(f"erro ao substituir caracteres em colunas do dataframe")

# deltaReplaceTable.printSchema()


# In[8]:


try:
    (
        deltaReplaceTable
            .write
            .format('delta')
            .mode('append')
            .save(f'{bronze}/accidents')
    )
except:
    print(f"erro ao salvar dados em {bronze}")

# In[ ]:


try:
    spark.stop()
except:
    print(f"erro ao finalizar a sessão do {spark}")
