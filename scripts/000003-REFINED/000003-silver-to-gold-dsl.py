#!/usr/bin/env python
# coding: utf-8

# In[ ]:


from pyspark.sql import SparkSession
from pyspark.sql.types import *
from pyspark.sql.functions import *
from delta import *
import databricks.koalas as ks


def display(x):
    df = ks.DataFrame(x)
    return df


core = 2
aplication = 'sparks.delta.application.refined'

deltaSession = (
    SparkSession
        .builder
        .appName(f'{aplication}')
        .master(f'local[{core}]')
        .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension")
        .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog")
)

spark = configure_spark_with_delta_pip(deltaSession).getOrCreate()

# areas do datalake
silver = '/home/romerito/Dropbox/tecnology/develop/python/spark-deltalake/datalake/000002-silver-zone'
gold = '/home/romerito/Dropbox/tecnology/develop/python/spark-deltalake/datalake/000003-gold-zone'

# In[ ]:


deltaSilverTable = (
    spark
        .read
        .format("delta")
        .option("path", f"{silver}/accidents")
        .load()
)

display(deltaSilverTable).head(5)

# In[ ]:


deltaColumns = (
    deltaSilverTable
        .withColumn("total_de_obitos", col("numero_de_obitos_tripulantes") + col("numero_obitos_passageiros"))
        .select(
        "tipo",
        "natureza",
        "data_ocorrencia_acidente",
        "ano_primeiro_voo",
        "anos_de_voo",
        "total_de_obitos",
        "dia_da_semana",
        "dia_do_acidente",
        "mes_do_acidente",
        "ano_do_acidente"
    )
)

display(deltaColumns).head(5)

# In[ ]:


deltaSummaryTable = (
    deltaColumns
        .groupBy(
        "ano_do_acidente",
        "mes_do_acidente",
        "dia_do_acidente",
        "dia_da_semana")
        .agg(
        sum("total_de_obitos")
            .alias("total_de_obitos")
    )

)

display(deltaSummaryTable).head(5)

# In[ ]:


(
    deltaSummaryTable
        .write
        .format("delta")
        .mode("overwrite")
        .save(f"{gold}/datamarth_obitos_por_periodo")
)
