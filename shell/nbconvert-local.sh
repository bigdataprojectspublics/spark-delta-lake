#!/bin/bash

export PATH_SCRIPTS="/home/romerito/Documents/bigdata/spark-delta-lake/scripts"
export PATH_NOTEBOOKS="/home/romerito/Documents/bigdata/spark-delta-lake/notebooks"

echo "convertendo notebooks de .ipynb para .py"
sudo jupyter nbconvert --output-dir="$PATH_SCRIPTS/000001-bronze" --to python $PATH_NOTEBOOKS/000001-landing-to-bronze-dsl.ipynb
sudo jupyter nbconvert --output-dir="$PATH_SCRIPTS/000002-silver" --to python $PATH_NOTEBOOKS/000002-bronze-to-silver-dsl.ipynb
sudo jupyter nbconvert --output-dir="$PATH_SCRIPTS/000003-gold" --to python $PATH_NOTEBOOKS/000003-silver-to-gold-dsl.ipynb

sudo chmod +x $PATH_SCRIPTS/* -R