#!/bin/bash

export PATH_RUNDECK="/home/rundeck"
export PATH_GITLAB="$PATH_RUNDECK/spark-delta-lake"
export PATH_NOTEBOOKS="$PATH_GITLAB/notebooks"
export OUTPUT_NOTEBOOKS="$PATH_GITLAB/scripts" 

echo "instalando dependencias..."
sudo pip3 install delta-spark
sudo pip3 install koalas

sudo rm -rf $PATH_GITLAB

echo "clonando projeto bigdataprojectspublics/spark-delta-lake do gitlab..."
cd $PATH_RUNDECK && git clone https://gitlab.com/bigdataprojectspublics/spark-delta-lake.git

sudo rm -rf "${OUTPUT_NOTEBOOKS}/000001-RAW/*"
sudo rm -rf "${OUTPUT_NOTEBOOKS}/000002-TRUSTED/*"
sudo rm -rf "${OUTPUT_NOTEBOOKS}/000003-REFINED/*"

echo "convertendo notebooks de .ipynb para .py"
sudo jupyter nbconvert --output-dir="$OUTPUT_NOTEBOOKS/000001-RAW" --to python $PATH_NOTEBOOKS/000001-landing-to-bronze-dsl.ipynb
sudo jupyter nbconvert --output-dir="$OUTPUT_NOTEBOOKS/000002-TRUSTED" --to python $PATH_NOTEBOOKS/000002-bronze-to-silver-dsl.ipynb
sudo jupyter nbconvert --output-dir="$OUTPUT_NOTEBOOKS/000003-REFINED" --to python $PATH_NOTEBOOKS/000003-silver-to-gold-dsl.ipynb

sudo chmod +x $OUTPUT_NOTEBOOKS/*